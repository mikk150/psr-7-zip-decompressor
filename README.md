ZIP inside PSR-7 StreamInterface decompressor
==========

Decompresses ZIP files inside PSR-7 StreamInterface

Installation:
-------------
The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require --prefer-dist mikk150/psr-7-zip-decompressor "dev-develop"
```

or add

```
"mikk150/psr-7-zip-decompressor": "dev-develop"
```
to the require section of your composer.json.

Usage:
------

```php
$stream = "<a PSR-7 message interface implemenation>";

$zipFileIterator = new Mikk150\Psr7ZipDecompressor($stream);

foreach ($zipFileIterator as $fileName => $fileStream) {
    echo $fileName;
    while (!$fileStream->eof()) {
        echo $fileStream->read(4096);
    }
}
```