<?php

namespace Mikk150\Psr7ZipDecompressor;

use Exception;
use Iterator;
use NoRewindIterator;
use Psr\Http\Message\StreamInterface;

class ZipFileIterator extends NoRewindIterator implements Iterator
{
    /**
     * @var StreamInterface
     */
    private $_stream;

    const LOCAL_FILE_HEADER = ZippedFile::LOCAL_FILE_HEADER;

    public function __construct(StreamInterface $stream)
    {
        $this->_stream = $stream;
    }

    private $_position = 0;

    private $_current;

    /**
     * @return ZippedFile
     */
    public function current(): mixed
    {
        if (!$this->_current) {
            if (!$this->valid()) {
                throw new Exception('Not PKZip file');
            }

            $currentPosition = $this->_stream->tell();

            $stream = new OffsetStream($this->_stream, $currentPosition + strlen(self::LOCAL_FILE_HEADER));

            $this->_current = new ZippedFile($stream);
        }
        return $this->_current;
    }

    public function next(): void
    {
        $this->_current = null;
        $this->_stream->seek(strlen(self::LOCAL_FILE_HEADER), SEEK_CUR);

        $lastHeaderBytes = '';

        $fileHasEnded = false;

        do {
            if (!($contents = $this->_stream->read(4096))) {
                $fileHasEnded = true;
            }

            $contents = $lastHeaderBytes . $contents;

            if (($fileHeaderLocation = strpos($contents, self::LOCAL_FILE_HEADER)) !== false) {
                $fullBatchLength = strlen($contents);
                $contents = substr($contents, 0, $fileHeaderLocation);
                $fileHasEnded = true;
                $this->_stream->seek(($fullBatchLength - $fileHeaderLocation) * -1, SEEK_CUR);
            }

            $lastHeaderBytes = substr($contents, strlen($contents) - (strlen(self::LOCAL_FILE_HEADER) -1));

        } while (!$fileHasEnded);
        $this->_position++;
    }

    public function key(): mixed
    {
        if ($this->valid()) {
            return $this->current()->getMetadata('fileName');
        }

        return null;
    }

    public function valid(): bool
    {

        $header = $this->_stream->read(strlen(self::LOCAL_FILE_HEADER));
        $valid = $header === self::LOCAL_FILE_HEADER;

        $this->_stream->seek(strlen(self::LOCAL_FILE_HEADER) * -1, SEEK_CUR);

        return $valid;
    }
}
