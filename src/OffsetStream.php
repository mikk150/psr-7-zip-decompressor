<?php

namespace Mikk150\Psr7ZipDecompressor;

use Exception;
use Psr\Http\Message\StreamInterface;
use RuntimeException;

class OffsetStream implements StreamInterface
{
    private $_stream;

    private $_offset;

    private $_size;

    private $_currentPos = 0;

    private $_parentStreamOffset = 0;

    public function __construct(StreamInterface $stream, int $offset = 0, $size = null)
    {
        if (!$stream->isSeekable()) {
            throw new Exception('`Parameter $stream has to be seekable`');
        }

        $this->_stream = $stream;
        $this->_parentStreamOffset = $this->_stream->tell();

        $this->_offset = $offset;

        $this->_size = $size;
    }

    public function __toString() { }

    public function close() { }

    public function detach() {
        return $this->_stream->detach();
    }

    public function getSize() {
        if ($this->_size) {
            return $this->_size;
        }

        if ($this->_stream->getSize()) {
            return $this->_stream->getSize() - $this->_offset;
        }
    }

    public function tell()
    {
        return $this->_currentPos;
    }

    public function eof() {
        if ($this->_stream->getSize()) {
            return $this->_offset + $this->_currentPos > $this->_stream->getSize();
        }

        $this->_stream->seek($this->_offset + $this->_currentPos);
        $eof = $this->_stream->eof();
        $this->_stream->seek($this->_parentStreamOffset);
        return $eof;
    }

    public function isSeekable() {
        return true;
    }

    public function seek($offset, $whence = SEEK_SET) {
        if ($whence === SEEK_SET) {
            if ($offset < 0) {
                throw new RuntimeException('$offset cannot be negative on whence SEEK_SET');
            }
            $this->_currentPos = $offset;
        }

        if ($whence === SEEK_CUR) {
            $this->_currentPos += $offset;
        }
    }

    public function rewind() {
        $this->_currentPos = 0;
    }

    public function isWritable() {
        return $this->_stream->isWritable();
    }

    public function write($string) { }

    public function isReadable() {
        return $this->_stream->isWritable();
    }

    public function read($length) {
        $this->_stream->seek($this->_offset + $this->_currentPos);

        $content = $this->_stream->read($length);

        $this->_currentPos += $length;

        $this->_stream->seek($this->_parentStreamOffset);

        return $content;
    }

    public function getContents() { }

    public function getMetadata($key = null) {
        return $this->_stream->getMetadata($key);
    }
}
