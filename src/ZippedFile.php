<?php

namespace Mikk150\Psr7ZipDecompressor;

use Psr\Http\Message\StreamInterface;

class ZippedFile implements StreamInterface
{
   const LOCAL_FILE_HEADER = "\x50\x4b\x03\x04";

   const CENTRAL_FILE_HEADER = "\x50\x4b\x01\x02";

   const CENTRAL_DIRECTORY_END = "\x50\x4b\x05\x06";

   const ZIP64_CENTRAL_DIRECTORY_LOCATOR = "\x50\x4b\x06\x07";

   const ZIP64_CENTRAL_DIRECTORY_END = "\x50\x4b\x06\x06";

   const DATA_DESCRIPTOR = "\x50\x4b\x07\x08";


    private $_version;
    private $_flag;
    private $_method;
    private $_modificationTime;
    private $_modificationDate;
    private $_crc;
    private $_compressedSize;
    private $_uncompressedSize;
    private $_filenameLength;
    private $_fileName;
    private $_extraFieldLength;
    private $_extraField;

    /**
     * @var StreamInterface
     */
    private $_localFileStream;

    /**
     * @var OffsetStream
     */
    private $_deflatedStream;
    public function __construct(StreamInterface $localFileStream)
    {
        $this->_localFileStream = $localFileStream;
        $this->fillExtraFields();

        $this->_deflatedStream = new OffsetStream($localFileStream, $this->getHeaderSize());
    }

    public function __toString()
    {
        return __CLASS__ . '(' . $this->_fileName . ')';
    }

    protected function fillExtraFields()
    {
        $contents = $this->_localFileStream->read(4096);

        $headers = unpack("vversion/vflag/vmethod/vmodificationTime/vmodificationDate/Vcrc/VcompressedSize/VuncompressedSize/vfilenameLength/vextraFieldLength", $contents);

        $this->_version = $headers['version'];
        $this->_flag = $headers['flag'];
        $this->_method = $headers['method'];
        $this->_modificationTime = $headers['modificationTime'];
        $this->_modificationDate = $headers['modificationDate'];
        $this->_crc = $headers['crc'];
        $this->_compressedSize = $headers['compressedSize'];
        $this->_uncompressedSize = $headers['uncompressedSize'];
        $this->_filenameLength = $headers['filenameLength'];
        $this->_extraFieldLength = $headers['extraFieldLength'];
        
        $this->_localFileStream->seek(26);
        
        $this->_fileName = $this->_localFileStream->read($this->_filenameLength);

        $this->_localFileStream->seek(26 + $this->_filenameLength);

        $this->_extraField = $this->_localFileStream->read($this->_extraFieldLength);
    }

    protected function getHeaderSize()
    {
        return 26 + $this->_filenameLength + $this->_extraFieldLength;
    }

    public function close() { }

    public function detach() { }

    public function getSize() {
        return $this->_uncompressedSize;
    }

    public function tell() { }

    public function eof()
    {
        return $this->_endOfFile;
    }

    public function isSeekable()
    {
        return false;
    }

    public function seek($offset, $whence = SEEK_SET) {

    }

    public function rewind() {

    }

    public function isWritable() {
        return false;
    }

    public function write($string) {
        return false;
    }

    public function isReadable() {
        return true;
    }

    private $_zlibContext;
    private $_inflatedBuffer = '';
    private $_lastHeaderBytes = '';
    private $_endOfFile = false;

    public function read($length) {

        if ($this->_endOfFile) {
            return '';
        }

        if (!$this->_zlibContext) {
            $this->_zlibContext = inflate_init(ZLIB_ENCODING_RAW);
        }

        while (strlen($this->_inflatedBuffer) < $length && !$this->eof()) {
            $content = $this->_deflatedStream->read($length);

            $lastHeaderBytes = substr($content, strlen($content) - strlen(self::LOCAL_FILE_HEADER));

            $content = $this->_lastHeaderBytes . $content;

            if ($this->_deflatedStream->eof()) {
                $this->_endOfFile = true;
            }


            if (($fileHeaderLocation = strpos($content, self::LOCAL_FILE_HEADER)) !== false) {
                $content = substr($content, 0, $fileHeaderLocation);

                $this->_endOfFile = true;
            }

            if ($this->_flag & (1 << 3) && ($fileHeaderLocation = strpos($content, self::DATA_DESCRIPTOR)) !== false) {
                $content = substr($content, 0, $fileHeaderLocation);
                $this->_endOfFile = true;
            }

            $content = substr($content, strlen($this->_lastHeaderBytes));

            $this->_inflatedBuffer .= inflate_add($this->_zlibContext, $content);

            $this->_lastHeaderBytes = $lastHeaderBytes;
        }

        $buffer = $this->_inflatedBuffer;

        $this->_inflatedBuffer = substr($buffer, $length);

        return substr($buffer, 0, $length);
    }

    public function getContents() {
        $this->_localFileStream->seek($this->getHeaderSize());
        return gzinflate($this->_localFileStream->getContents());
    }

    public function getMetadata($key = null)
    {
        $metaData = $this->_localFileStream->getMetadata($key);

        $metaData['fileName'] = $this->_fileName;

        if ($key === null) {
            return $metaData;
        }
        return $metaData[$key] ?? null;
    }
}
